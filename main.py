"""Simple implementation of the Chord protocol (D&NP lab06)

Usage:
    usage: main.py [-h] [-registry_port PORT] BITS FIRST_PORT LAST_PORT

    Simple implementation of the Chord protocol (D&NP lab06)

    positional arguments:
    BITS                 length of identifiers in chord (in bits)
    FIRST_PORT           port range for nodes
    LAST_PORT            port range for nodes

    optional arguments:
    -h, --help           show help message and exit
    -registry_port PORT  port for registry



Examples:
    Use default value for the registry port (25565)
    $ python main.py 6 5000 5004

    Specify value of the registry port
    $ python main.py -m 6 -registry_port 25566 5000 5004
"""


import argparse
import random
import time
import zlib

from node import Node
from registry import Registry
from proxy import NodeProxy, RegistryProxy


def parse_args(args=None):
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description='Simple implementation of the Chord protocol (D&NP lab06)',
        add_help=True,
    )

    parser.add_argument('m', type=int, metavar='BITS', help='length of identifiers in chord (in bits)')
    parser.add_argument('first_port', type=int, metavar='FIRST_PORT', help='port range for nodes')
    parser.add_argument('last_port', type=int, metavar='LAST_PORT', help='port range for nodes')
    parser.add_argument('-registry_port', type=int, metavar='PORT', default=25565, help='port for registry')
    return parser.parse_args(args)


def data2file_identifier(data: bytes, m: int) -> int:
    return zlib.adler32(data) % (2 ** m)


def main():
    args = parse_args()
    random.seed(0)

    registry = Registry(('localhost', args.registry_port), args.m)
    registry.start()

    nodes = [Node(('localhost', p), f'http://localhost:{args.registry_port}') for p in range(args.first_port, args.last_port + 1)]
    for n in nodes:
        n.start()
        time.sleep(0.05)  # just to get the results as in the example

    registryProxy = RegistryProxy(f'http://localhost:{registry.port}')
    nodesProxy = {n.port: NodeProxy(f'http://localhost:{n.port}') for n in nodes}

    print('Registry and 5 nodes are created.')

    def parse_input(cmdline: str):

        def parse_port(port: str) -> int:
            try:
                res = int(port)
            except ValueError as e:
                raise ValueError('Incorrect port number') from e
            if res not in nodesProxy:
                raise ValueError(f'Node with port {res} isn’t available')
            return res

        cmd = cmdline.split()
        if not cmd:
            return
        if cmd[0] == 'get_chord_info':
            print(registryProxy.get_chord_info())
        elif cmd[0] == 'get_finger_table' and len(cmd) == 2:
            port = parse_port(cmd[1])
            print(nodesProxy[port].get_finger_table())
        elif cmd[0] == 'save' and len(cmd) == 3:
            port = parse_port(cmd[1])
            print(f'{cmd[2]} has identifier {data2file_identifier(cmd[2].encode(), args.m)}')
            print(nodesProxy[port].savefile(cmd[2]))
        elif cmd[0] == 'get' and len(cmd) == 3:
            print(f'{cmd[2]} has identifier {data2file_identifier(cmd[2].encode(), args.m)}')
            port = parse_port(cmd[1])
            print(nodesProxy[port].getfile(cmd[2]))
        elif cmd[0] == 'quit' and len(cmd) == 2:
            port = parse_port(cmd[1])
            # TODO: если ноды не существует, то писать tuple, а не ValueError
            print(nodesProxy[port].quit())
            del nodesProxy[port]
        else:
            raise ValueError(f'Unknown command. Try again')

    try:
        while True:
            try:
                parse_input(input('> '))
            except EOFError:
                print('\nEOFError')
                break
            except Exception as e:
                print(repr(e))
    except KeyboardInterrupt:
        print('\nKeyboardInterrupt')
    finally:
        for np in nodesProxy.values():
            np.quit()

        registry.stop()
        registry.join()

        for n in nodes:
            n.join()


if __name__ == '__main__':
    main()

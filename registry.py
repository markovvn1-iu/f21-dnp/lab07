import random
from bisect import bisect_left
from threading import Thread
from typing import Dict, Tuple
from xmlrpc.server import SimpleXMLRPCServer


class Registry(Thread):

    _working: bool = False

    _m: int  # key length (in bits)
    _max_nodes: int
    _nodes_info: Dict[int, int] = {}  # {node_id: port}

    _server_addr: Tuple[str, int]
    _server: SimpleXMLRPCServer

    def __init__(self, server_addr: Tuple[str, int], m: int):
        super().__init__()
        self._m = m
        self._max_nodes = 2 ** m

        self._server_addr = server_addr
        self._server = SimpleXMLRPCServer(server_addr, logRequests=False)
        self._server.timeout = 0.1
        self._server.register_introspection_functions()
        self._server.register_function(self.register)
        self._server.register_function(self.deregister)
        self._server.register_function(self.get_chord_info)
        self._server.register_function(self.populate_finger_table)

    @property
    def port(self) -> int:
        """Port of the registry"""
        return self._server_addr[1]

    def register(self, port: int) -> Tuple[int, str, int]:
        """Register new node

        Method is registered as RPC service at predefined port and can be invoked by new
        node to register itself. It is responsible to register the new node with given
        port number, i.e., assigns id from identifier space. id is randomly chosen from
        [0, 2m-1] range. Use random module and initialize it to seed=0 so your results
        will be the same as reference output! If newly generated id is the same as
        existing id, it again randomly selects another number until no collision
         * If successful, shall return tuple of (node’s assigned id, optional message about the
        network size)
         * If unsuccessful, shall return tuple of (-1, error message explaining why it was unsuccessful)
           - It fails when for example when the Chord is full, i.e., there are already 2m nodes given
             that m is the identifier length in bits

        Args:
            port (int): port of the node

        Returns:
            Tuple[int, str]: (node id, status message, network size m)
        """
        if len(self._nodes_info) >= self._max_nodes:
            return -1, 'network is full', self._m

        while True:
            node_id = random.randint(0, self._max_nodes - 1)
            if node_id not in self._nodes_info:
                break

        self._nodes_info[node_id] = port
        return node_id, 'network is not full', self._m

    def deregister(self, node_id: int) -> Tuple[bool, str]:
        """Deregister node

        Method is also registered as RPC service at predefined port and can be invoked
        by the registered node to deregister itself. It is responsible to deregister the
        node with given id. Returns tuple
         * (True, success message) upon successfully deregistering of the node with given id
         * (False, error message) upon failure. EX) no such id exists in dict of registered nodes

        Args:
            node_id (int): id of the node

        Returns:
            Tuple[bool, str]: (success status, status message)
        """
        if node_id not in self._nodes_info:
            return False, f'Node with id {node_id} is not registered'
        del self._nodes_info[node_id]
        return True, f'Node with id {node_id} is successfully deregistered'

    def get_chord_info(self) -> Dict[str, int]:
        """Get info of the chord system

        Method is also registered as RPC service and usually is invoked by your main
        program to get the information about chord: dict of node ids and port numbers.

        Returns:
            Dict[str, int]: dictionary of all nodes: {node_id: port}
        """
        return {str(k): v for k, v in self._nodes_info.items()}

    def populate_finger_table(self, node_id: int) -> Tuple[Dict[str, int], Tuple[int, int]]:
        """Populate finder table and node predecessor

        Method is responsible to generate the dict of the (id, port number) pairs that the
        requesting node can directly communicate with. It is registered as RPC service and
        can be invoked by the nodes.
        This method also returns the (id, port) of the predecessor node - the next node reverse
        clockwise. For example, if there are no nodes between nodes with ids 31 and 2, then
        predecessor of node 2 is node 31. So this method returns tuple in addition to dict of finger
        table.

        Args:
            node_id (int): id of the node

        Returns:
            Tuple[Dict[str, int], Tuple[int, int]]: Finder table and (id, port) of the node predecessor
        """
        all_nodes = sorted(list(self._nodes_info.keys()))
        succ = lambda x: all_nodes[bisect_left(all_nodes, x) % len(all_nodes)]  # get successor for node x
        pred = lambda x: all_nodes[(bisect_left(all_nodes, x) - 1) % len(all_nodes)]  # get predecessor for node x

        res_ids = [succ((node_id + 2 ** i) % self._max_nodes) for i in range(self._m)]
        res_pred_id = pred(node_id)
        return {str(i): self._nodes_info[i] for i in res_ids}, (res_pred_id, self._nodes_info[res_pred_id])

    def run(self):
        self._working = True
        while self._working:
            self._server.handle_request()

        self._server.server_close()

    def stop(self):
        self._working = False

from typing import Dict
import pytest

from registry import Registry

@pytest.fixture(name='registry')
def _registry():
    registry = Registry(('localhost', 25565), 5)
    yield registry
    registry._server.server_close()


# @pytest.fixture(name='registry_with_nodes1')
# def _registry_with_nodes1(registry: Registry):
#     registry._nodes_info = {1: 5000, 26: 5001, 2: 5002, 16: 5003, 31: 5004}
#     return registry


@pytest.fixture(name='registry_with_nodes2')
def _registry_with_nodes2(registry: Registry):
    # Example from lab
    registry._nodes_info = {1: 0, 4: 1, 9: 2, 11: 3, 14: 4, 18: 5, 20: 6, 21: 7, 28: 8}
    return registry


@pytest.mark.parametrize('node_id, res_finger_table', [
    (1, {'4': 1, '9': 2, '18': 5}),
    (4, {'9': 2, '14': 4, '20': 6}),
    (9, {'11': 3, '14': 4, '18': 5, '28': 8}),
    (11, {'14': 4, '18': 5, '20': 6, '28': 8}),
    (14, {'18': 5, '28': 8, '1': 0}),
    (18, {'20': 6, '28': 8, '4': 1}),
    (20, {'21': 7, '28': 8, '4': 1}),
    (21, {'28': 8, '1': 0, '9': 2}),
    (28, {'1': 0, '4': 1, '14': 4}),
])
def test_registry1_finger_table(registry_with_nodes2: Registry, node_id: int, res_finger_table: Dict[str, int]):
    assert registry_with_nodes2.populate_finger_table(node_id)[0] == res_finger_table


@pytest.mark.parametrize('node_id, res_pred', [
    (1, (28, 8)),
    (4, (1, 0)),
    (9, (4, 1)),
    (11, (9, 2)),
    (14, (11, 3)),
    (18, (14, 4)),
    (20, (18, 5)),
    (21, (20, 6)),
    (28, (21, 7)),
])
def test_registry1_pred(registry_with_nodes2: Registry, node_id: int, res_pred: Dict[str, int]):
    assert registry_with_nodes2.populate_finger_table(node_id)[1] == res_pred

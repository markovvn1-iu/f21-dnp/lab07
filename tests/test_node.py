import pytest

from node import Node

@pytest.fixture(name='node')
def _node():
    node = Node(('localhost', 25565), 'http://localhost:25565')
    node._working = True
    node._m = 5
    yield node
    node._server.server_close()


@pytest.fixture(name='node_1')
def _node_1(node: Node):
    node._node_id = 1
    node._finger_table = {4: 0, 9: 0, 18: 0}
    node._predecessor = (28, 0)
    node._update_node_info()
    return node


@pytest.fixture(name='node_21')
def _node_21(node: Node):
    node._node_id = 21
    node._finger_table = {28: 0, 1: 0, 9: 0}
    node._predecessor = (20, 0)
    node._update_node_info()
    return node


@pytest.mark.parametrize('look_id, res_id', [
    (29, 1),
    (31, 1),
    (0, 1),
    (1, 1),
    (2, 4),
    (3, 4),
    (4, 4),
    (5, 4),
    (8, 4),
    (9, 9),
    (10, 9),
    (17, 9),
    (18, 18),
    (19, 18),
    (27, 18),
    (28, 18)
])
def test_node_1_lookup(node_1: Node, look_id: int, res_id: int):
    assert node_1._lookup_node(look_id) == res_id


@pytest.mark.parametrize('look_id, res_id', [
    (21, 21),
    (22, 28),
    (25, 28),
    (28, 28),
    (29, 28),
    (31, 28),
    (0, 28),
    (1, 1),
    (2, 1),
    (8, 1),
    (9, 9),
    (20, 9),
])
def test_node_21_lookup(node_21: Node, look_id: int, res_id: int):
    assert node_21._lookup_node(look_id) == res_id
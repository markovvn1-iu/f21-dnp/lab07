import time
from typing import Dict, List, Tuple
import pytest

from node import Node
from registry import Registry
from proxy import RegistryProxy, NodeProxy


@pytest.fixture(name='system')
def _system(registry_port=25565, m=5, first_port=5000, last_port=5004) -> Tuple[Registry, Dict[int, Node]]:
    registry = Registry(('localhost', registry_port), m)
    registry.start()

    nodes = [Node(('localhost', p), f'http://localhost:{registry_port}') for p in range(first_port, last_port + 1)]
    for n in nodes:
        n.start()

    time.sleep(1.5)  # wit until all nodes will be ready
    yield registry, {n.port: n for n in nodes}

    for n_port in list(registry.get_chord_info().values()):
        print(n_port)
        node = NodeProxy(f'http://localhost:{n_port}')
        node.quit()

    registry.stop()
    registry.join()

    for n in nodes:
        n.join()


@pytest.fixture(name='system_proxy')
def _system_proxy(system: Tuple[Registry, Dict[int, Node]]) -> Tuple[Tuple[Registry, Dict[int, Node]], Tuple[RegistryProxy, Dict[int, NodeProxy]]]:
    registryProxy = RegistryProxy(f'http://localhost:{system[0].port}')
    nodesProxy = {n.port: NodeProxy(f'http://localhost:{n.port}') for n in system[1].values()}
    return system, (registryProxy, nodesProxy)


def test_registry_save_twice(system_proxy: Tuple[Tuple[Registry, Dict[int, Node]], Tuple[RegistryProxy, Dict[int, NodeProxy]]]):
    (registry, nodes), (registryProxy, nodesProxy) = system_proxy

    status, _ = nodesProxy[5000].savefile('Kazan')
    assert status
    status, _ = nodesProxy[5000].savefile('Kazan')
    assert not status

    status, _ = nodesProxy[5000].getfile('Kazan')
    assert status
    status, _ = nodesProxy[5001].getfile('Kazan')
    assert status
    status, _ = nodesProxy[5004].getfile('Kazan')
    assert status


def test_registry_no_file(system_proxy: Tuple[Tuple[Registry, Dict[int, Node]], Tuple[RegistryProxy, Dict[int, NodeProxy]]]):
    (registry, nodes), (registryProxy, nodesProxy) = system_proxy

    status, _ = nodesProxy[5000].getfile('Kazan')
    assert not status
    status, _ = nodesProxy[5002].getfile('kkk')
    assert not status
    status, _ = nodesProxy[5003].getfile('Kazan')
    assert not status



def test_registry_remove_nodes(system_proxy: Tuple[Tuple[Registry, Dict[int, Node]], Tuple[RegistryProxy, Dict[int, NodeProxy]]]):
    (registry, nodes), (registryProxy, nodesProxy) = system_proxy

    status, _ = nodesProxy[5002].savefile('Kazan')
    assert status
    status, _ = nodesProxy[5004].savefile('Rostov')
    assert status
    
    status, _ = nodesProxy[5001].quit()
    assert status
    status, _ = nodesProxy[5002].quit()
    assert status
    status, _ = nodesProxy[5003].quit()
    assert status
    status, _ = nodesProxy[5004].quit()
    assert status

    status, _ = nodesProxy[5000].getfile('Kazan')
    assert status
    status, _ = nodesProxy[5000].getfile('Kazan')
    assert status
from xmlrpc.client import ServerProxy
from typing import Any, Dict, List, Tuple


class NodeProxy:
    """Proxy for the Node
    
    See Node class for more details
    """

    _proxy: ServerProxy

    def __init__(self, *args: Any, **kwargs: Any):
        self._proxy = ServerProxy(*args, **kwargs)

    def get_finger_table(self) -> Dict[int, int]:
        return {int(k): v for k, v in self._proxy.get_finger_table().items()}

    def savefile(self, filename: str) -> Tuple[bool, str]:
        return self._proxy.savefile(filename)

    def getfile(self, filename: str) -> Tuple[bool, str]:
        return self._proxy.getfile(filename)

    def _predecessor_quit(self, storage: List[str], new_predecessor: Tuple[int, int]) -> bool:
        return self._proxy._predecessor_quit(storage, new_predecessor)

    def _successor_quit(self, new_successor: Tuple[int, int]) -> bool:
        return self._proxy._successor_quit(new_successor)

    def quit(self) -> Tuple[bool, str]:
        return self._proxy.quit()


class RegistryProxy:
    """Proxy for the Registry
    
    See Registry class for more details
    """

    _proxy: ServerProxy

    def __init__(self, *args: Any, **kwargs: Any):
        self._proxy = ServerProxy(*args, **kwargs)

    def register(self, port) -> Tuple[int, str, int]:
        return self._proxy.register(port)

    def deregister(self, node_id) -> Tuple[bool, str]:
        return self._proxy.deregister(node_id)

    def get_chord_info(self) -> Dict[int, int]:
        return {int(k): v for k, v in self._proxy.get_chord_info().items()}

    def populate_finger_table(self, node_id: int) -> Tuple[Dict[int, int], Tuple[int, int]]:
        res = self._proxy.populate_finger_table(node_id)
        return {int(k): v for k, v in res[0].items()}, res[1]

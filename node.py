import time
import zlib
from bisect import bisect_right
from threading import Thread
from typing import Any, Dict, List, Set, Tuple
from xmlrpc.server import SimpleXMLRPCServer

from proxy import NodeProxy, RegistryProxy



class Node(Thread):

    _working: bool = False

    _m: int = 0  # network size parameter
    _node_id: int = -1
    _server_addr: Tuple[str, int]
    _server: SimpleXMLRPCServer
    _registry: RegistryProxy

    _finger_table_nodes: List[int]
    _finger_table: Dict[int, int]  # {node_id: port}
    _successor_id: int
    _predecessor: Tuple[int, int]

    _storage: Set[str]

    def __init__(self, server_addr: Tuple[str, int], registry_uri: str):
        super().__init__()
        self._storage = set()

        self._server_addr = server_addr
        self._registry = RegistryProxy(registry_uri)

        self._server = SimpleXMLRPCServer(server_addr, logRequests=False)
        self._server.timeout = 0.1
        self._server.register_introspection_functions()
        self._server.register_function(self.get_finger_table)
        self._server.register_function(self.savefile)
        self._server.register_function(self.getfile)
        self._server.register_function(self._predecessor_quit)
        self._server.register_function(self._successor_quit)
        self._server.register_function(self.quit)

    @property
    def port(self) -> int:
        return self._server_addr[1]

    def get_finger_table(self) -> Dict[str, int]:
        return {str(k): v for k, v in self._finger_table.items()}

    def _data2node_id(self, data: bytes) -> int:
        return zlib.adler32(data) % (2 ** self._m)

    def _lookup_node(self, target_id: int) -> int:
        # If the target is stored in this node
        if self._predecessor[0] == self._node_id:
            return self._node_id
        if self._predecessor[0] < target_id and target_id <= self._node_id:
            return self._node_id
        if self._predecessor[0] > self._node_id and (target_id <= self._node_id or target_id > self._predecessor[0]):
            return self._node_id

        # If the target is stored in the next node
        if self._node_id < target_id and target_id <= self._successor_id:
            return self._successor_id
        if self._node_id > self._successor_id and (target_id <= self._successor_id or target_id > self._node_id):
            return self._successor_id

        # Find the best node to continue lookup
        a = self._finger_table_nodes
        return a[bisect_right(a, target_id) - 1]         

    def savefile(self, filename: str) -> Tuple[bool, str]:
        target_id = self._data2node_id(filename.encode())
        target_node_id = self._lookup_node(target_id)

        # save at this node
        if target_node_id == self._node_id:
            if filename in self._storage:
                return False, f'{filename} already exists in Node {self._node_id}'
            self._storage.add(filename)
            return True, f'{filename} is saved in Node {self._node_id}'

        print(f'node {self._node_id} passed request to node {target_node_id}')

        # connect to next node and ask to save the file
        node = NodeProxy(f'http://localhost:{self._finger_table[target_node_id]}')
        return node.savefile(filename)

    def getfile(self, filename: str) -> Tuple[bool, str]:
        target_id = self._data2node_id(filename.encode())
        target_node_id = self._lookup_node(target_id)

        # get at this node
        if target_node_id == self._node_id:
            if filename in self._storage:
                return True, f'Node {self._node_id} has {filename}'
            else:
                return False, f'Node {self._node_id} doesn’t have the {filename}'

        print(f'node {self._node_id} passed request to node {target_node_id}')

        # connect to next node and ask to get the file
        node = NodeProxy(f'http://localhost:{self._finger_table[target_node_id]}')
        return node.getfile(filename)

    def _predecessor_quit(self, storage: List[str], new_predecessor: Tuple[int, int]) -> bool:
        self._storage |= set(storage)
        self._predecessor = new_predecessor
        return True

    def _successor_quit(self, new_successor: Tuple[int, int]) -> bool:
        self._finger_table.pop(self._successor_id)
        self._finger_table[new_successor[0]] = new_successor[1]
        self._update_node_info()
        return True

    def quit(self) -> Tuple[bool, str]:
        self._update_finger_table()  # update finder table before quit (to get correct nodes)

        if self._successor_id != self._node_id:
            succ_node = NodeProxy(f'http://localhost:{self._finger_table[self._successor_id]}')
            succ_node._predecessor_quit(list(self._storage), self._predecessor)

        if self._predecessor[0] != self._node_id:
            pred_node = NodeProxy(f'http://localhost:{self._predecessor[1]}')
            pred_node._successor_quit((self._successor_id, self._finger_table[self._successor_id]))

        status, _ = self._registry.deregister(self._node_id)
        if not status:
            return False, f'Node {self._node_id} with port {self.port} isn’t part of the network'
        self._working = False
        return True, f'Node {self._node_id} with port {self.port} was successfully removed'

    def _update_node_info(self):
        self._finger_table_nodes = sorted(list(self._finger_table.keys()))

        a = self._finger_table_nodes
        self._successor_id = a[bisect_right(a, self._node_id) % len(a)]

    def _update_finger_table(self):
        self._finger_table, self._predecessor = self._registry.populate_finger_table(self._node_id)
        self._update_node_info()

    def run(self):
        self._working = True
        self._node_id, _, self._m = self._registry.register(self._server_addr[1])
        time.sleep(1)

        last_update_time = time.monotonic_ns()
        self._update_finger_table()

        while self._working:
            self._server.handle_request()

            c_time = time.monotonic_ns()
            if c_time - last_update_time > 1e9:
                self._update_finger_table()
                last_update_time = c_time

        self._server.server_close()
